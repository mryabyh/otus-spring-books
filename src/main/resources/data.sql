INSERT INTO GENRE (id, name, last_update)
VALUES (genre_seq.nextval, 'Драма', 1000);
INSERT INTO GENRE (id, name, last_update)
VALUES (genre_seq.nextval, 'Поэма', 1000);
INSERT INTO GENRE (id, name, last_update)
VALUES (genre_seq.nextval, 'Былина', 1000);

INSERT INTO AUTHOR (ID, FIRST_NAME, LAST_NAME, FULL_NAME, LAST_UPDATE)
VALUES (author_seq.nextval, 'Аркадий', 'Укупник', 'А.Укупник', 2000);
INSERT INTO AUTHOR (ID, FIRST_NAME, LAST_NAME, FULL_NAME, LAST_UPDATE)
VALUES (author_seq.nextval, 'Семён', 'Фарада', 'С.Фарада', 2000);
INSERT INTO AUTHOR (ID, FIRST_NAME, LAST_NAME, FULL_NAME, LAST_UPDATE)
VALUES (author_seq.nextval, 'Александр', 'Македонский', 'А.Македонский', 2000);

INSERT INTO BOOK(ID, AUTHOR_ID, GENRE_ID, NAME, YEAR)
values (book_seq.nextval, (select id from AUTHOR where FULL_NAME = 'А.Укупник'),
        (select id from GENRE where name = 'Драма'), 'Драма жизни', 2000);

INSERT INTO BOOK(ID, AUTHOR_ID, GENRE_ID, NAME, YEAR)
values (book_seq.nextval, (select id from AUTHOR where FULL_NAME = 'С.Фарада'),
        (select id from GENRE where name = 'Поэма'), 'Песнь о вещем Олеге', 1980);

INSERT INTO BOOK(ID, AUTHOR_ID, GENRE_ID, NAME, YEAR)
values (book_seq.nextval, (select id from AUTHOR where FULL_NAME = 'А.Македонский'),
        (select id from GENRE where name = 'Драма'), 'Моя жизнь', 100);