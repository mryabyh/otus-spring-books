package ru.otus.dao;

import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.stereotype.Repository;
import ru.otus.domain.Author;
import ru.otus.domain.Genre;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class JdbcGenreDao implements GenreDao {

    private final NamedParameterJdbcOperations jdbc;
    private final GenreMapper mapper;

    public JdbcGenreDao(NamedParameterJdbcOperations jdbc) {
        this.jdbc = jdbc;
        mapper = new GenreMapper();
    }

    @Override
    public List<Genre> getGenrePage(Pageable page) {
        return null;
    }

    @Override
    public List<Genre> getAllGenre() {
        return jdbc.query("Select ID gid, NAME gname, LAST_UPDATE gdate from GENRE g", mapper);
    }

    @Override
    public Genre getGenreById(Long id) {
        final Map<String, Object> param = new HashMap<>(1);
        param.put("id", id);
        return jdbc.queryForObject("Select ID gid, NAME gname, LAST_UPDATE gdate from GENRE g where g.id = :id", param, mapper);
    }

    @Override
    public void addGenre(String name) {
        final Map<String, Object> param = new HashMap<>(1);
        param.put("name", name);
        param.put("lastUpdate", new Date().getTime());
        jdbc.update("INSERT INTO GENRE (id, name, last_update) VALUES (genre_seq.nextval, :name, :lastUpdate)", param);
    }

    public static class GenreMapper implements RowMapper<Genre> {
        @Override
        public Genre mapRow(ResultSet resultSet, int i) throws SQLException {
            return new Genre(resultSet.getLong("gid"),
                    resultSet.getString("gname"),
                    resultSet.getLong("gdate"));
        }
    }
}
