package ru.otus.dao;

import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.stereotype.Repository;
import ru.otus.domain.Book;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class JdbcBookDao implements BookDao {

    private final NamedParameterJdbcOperations jdbc;
    private final BookMapper mapper;

    public JdbcBookDao(NamedParameterJdbcOperations jdbc) {
        this.jdbc = jdbc;
        mapper = new BookMapper();
    }

    @Override
    public List<Book> getBooksPage(Pageable page) {
        return null;
    }

    @Override
    public List<Book> getAllBooks() {
        return jdbc.query("Select b.ID bid, b.NAME bname, b.LAST_UPDATE bdate, b.year byear, " +
                "g.ID gid, g.NAME gname, g.LAST_UPDATE gdate, " +
                "a.ID aid, a.FIRST_NAME afirstname, a.LAST_NAME alastname, a.FULL_NAME afullname, a.LAST_UPDATE adate " +
                "FROM book b INNER JOIN author a ON a.id = b.author_id INNER JOIN genre g ON g.id = b.genre_id", mapper);
    }

    @Override
    public Book getBookById(Long id) {
        final Map<String, Object> param = new HashMap<>(1);
        param.put("id", id);
        return jdbc.queryForObject("Select b.ID bid, b.NAME bname, b.LAST_UPDATE bdate, b.year byear, " +
                "g.ID gid, g.NAME gname, g.LAST_UPDATE gdate, " +
                "a.ID aid, a.FIRST_NAME afirstname, a.LAST_NAME alastname, a.FULL_NAME afullname, a.LAST_UPDATE adate " +
                "FROM book b INNER JOIN author a ON a.id = b.author_id INNER JOIN genre g ON g.id = b.genre_id WHERE b.id=:id LIMIT 1", param, mapper);
    }

    @Override
    public void addBook(String name, Integer year, Long authorId, Long genreId) {
        final Map<String, Object> param = new HashMap<>(5);
        param.put("name", name);
        param.put("year", year);
        param.put("lastUpdate", new Date().getTime());
        param.put("authorId", authorId);
        param.put("genreId", genreId);
        try {
            jdbc.update("INSERT INTO BOOK (id, name, last_update, author_id, genre_id, year) VALUES (book_seq.nextval, :name, :lastUpdate, :authorId, :genreId, :year)", param);
        } catch (DataAccessException e) {
            System.out.println(String.format("Невозможно добавить книгу с заданными параметрами: name %s, year %d authorId %s, genreId %s", name, year, authorId, genreId));
        }
    }

    public class BookMapper implements RowMapper<Book> {
        @Override
        public Book mapRow(ResultSet resultSet, int i) throws SQLException {
            return new Book(resultSet.getLong("bid"),
                    new JdbcAuthorDao.AuthorMapper().mapRow(resultSet, i),
                    new JdbcGenreDao.GenreMapper().mapRow(resultSet, i),
                    resultSet.getString("bname"),
                    resultSet.getInt("byear"),
                    resultSet.getLong("bdate"));
        }
    }
}
