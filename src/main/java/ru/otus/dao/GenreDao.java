package ru.otus.dao;

import org.springframework.data.domain.Pageable;
import ru.otus.domain.Genre;

import java.util.List;

public interface GenreDao {
    List<Genre> getGenrePage(Pageable page);

    List<Genre> getAllGenre();

    Genre getGenreById(Long id);

    void addGenre(String name);
}
