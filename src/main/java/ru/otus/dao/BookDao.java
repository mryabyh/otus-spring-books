package ru.otus.dao;

import org.springframework.data.domain.Pageable;
import ru.otus.domain.Book;

import java.util.List;

public interface BookDao {
    List<Book> getBooksPage(Pageable page);

    List<Book> getAllBooks();

    Book getBookById(Long id);

    void addBook(String name, Integer year, Long authorId, Long genreId);
}
