package ru.otus.dao;

import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.stereotype.Repository;
import ru.otus.domain.Author;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class JdbcAuthorDao implements AuthorDao {

    private final NamedParameterJdbcOperations jdbc;
    private final AuthorMapper mapper;

    public JdbcAuthorDao(NamedParameterJdbcOperations jdbc) {
        this.jdbc = jdbc;
        mapper = new AuthorMapper();
    }

    @Override
    public List<Author> getAuthorPage(Pageable page) {
        return null;
    }

    @Override
    public List<Author> getAllAuthor() {
        return jdbc.query("Select ID aid, FIRST_NAME afirstname, LAST_NAME alastname, FULL_NAME afullname, LAST_UPDATE adate from AUTHOR a", mapper);
    }

    @Override
    public Author getAuthorById(Long id) {
        final Map<String, Object> param = new HashMap<>(1);
        param.put("id", id);
        return jdbc.queryForObject("Select ID aid, FIRST_NAME afirstname, LAST_NAME alastname, FULL_NAME afullname, LAST_UPDATE adate from AUTHOR a where a.id = :id", param, mapper);
    }

    @Override
    public void addAuthor(String firstName, String lastName, String fullName) {
        final Map<String, Object> param = new HashMap<>(1);
        param.put("firstName", firstName);
        param.put("lastName", lastName);
        param.put("fullName", fullName);
        param.put("lastUpdate", new Date().getTime());
        jdbc.update("INSERT INTO AUTHOR (ID, FIRST_NAME, LAST_NAME, FULL_NAME, LAST_UPDATE) " +
                "VALUES (author_seq.nextval, :firstName, :lastName, :fullName, :lastUpdate)", param);
    }

    public static class AuthorMapper implements RowMapper<Author> {
        @Override
        public Author mapRow(ResultSet resultSet, int i) throws SQLException {
            return new Author(resultSet.getLong("aid"),
                    resultSet.getString("afirstname"),
                    resultSet.getString("alastname"),
                    resultSet.getString("afullname"),
                    resultSet.getLong("adate"));
        }
    }
}
