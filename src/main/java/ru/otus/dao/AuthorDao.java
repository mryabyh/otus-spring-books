package ru.otus.dao;

import org.springframework.data.domain.Pageable;
import ru.otus.domain.Author;

import java.util.List;

public interface AuthorDao {

    List<Author> getAuthorPage(Pageable page);

    List<Author> getAllAuthor();

    Author getAuthorById(Long id);

    void addAuthor(String firstName, String lastName, String fullName);
}
