package ru.otus.shell;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;
import ru.otus.dao.AuthorDao;
import ru.otus.dao.BookDao;
import ru.otus.dao.GenreDao;
import ru.otus.domain.Author;
import ru.otus.domain.Book;
import ru.otus.domain.Genre;

import java.util.List;
import java.util.stream.Collectors;

@ShellComponent
public class ShellCommands {

    private final BookDao booksDao;
    private final AuthorDao authorDao;
    private final GenreDao genreDao;

    @Autowired
    public ShellCommands(BookDao booksDao, AuthorDao authorDao, GenreDao genreDao) {
        this.booksDao = booksDao;
        this.authorDao = authorDao;
        this.genreDao = genreDao;
    }

    @ShellMethod("Get all books")
    public String getAllBooks() {
        final List<Book> allBooks = booksDao.getAllBooks();
        return allBooks.stream().map(Book::toString).collect(Collectors.joining("\r\n"));
    }

    @ShellMethod("Get all authors")
    public String getAllAuthors() {
        final List<Author> allAuthor = authorDao.getAllAuthor();
        return allAuthor.stream().map(Author::toString).collect(Collectors.joining("\r\n"));
    }

    @ShellMethod("Get all genres")
    public String getAllGenres() {
        final List<Genre> allGenre = genreDao.getAllGenre();
        return allGenre.stream().map(Genre::toString).collect(Collectors.joining("\r\n"));
    }

    @ShellMethod("Add genre. Params:String name")
    public void addGenre(@ShellOption String name) {
        genreDao.addGenre(name);
    }

    @ShellMethod("Add author. Params:String firstName, String lastName, String fullName")
    public void addAuthor(@ShellOption String firstName, @ShellOption String lastName, @ShellOption String fullName) {
        authorDao.addAuthor(firstName, lastName, fullName);
    }

    @ShellMethod("Add book. Params:String name, Integer year, Long authorId, Long genreId")
    public void addBook(@ShellOption String name, @ShellOption Integer year, @ShellOption Long authorId, @ShellOption Long genreId) {
        booksDao.addBook(name, year, authorId, genreId);
    }

    @ShellMethod("Get author by id")
    public String getAuthorById(@ShellOption Long id) {
        return authorDao.getAuthorById(id).toString();
    }

    @ShellMethod("Get genre by id")
    public String getGenreById(@ShellOption Long id) {
        return genreDao.getGenreById(id).toString();
    }

    @ShellMethod("Get book by id")
    public String getBookById(@ShellOption Long id) {
        return booksDao.getBookById(id).toString();
    }
}
