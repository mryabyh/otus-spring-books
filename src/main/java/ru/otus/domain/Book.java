package ru.otus.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Book {

    private Long id;
    private Author author;
    private Genre genre;
    private String name;
    private Integer year;
    private Long lastUpdate;
}
