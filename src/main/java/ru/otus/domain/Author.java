package ru.otus.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Author {

    private Long id;
    private String firstName;
    private String lastName;
    private String fullName;
    private Long lastUpdate;
}
